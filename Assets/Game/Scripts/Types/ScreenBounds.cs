﻿/// <summary>
/// Bounds of the screen
/// </summary>
public static class ScreenBounds {
  public const float YMin = (float) -5.2;
  public const float YMax = (float) 5.2;
  public const float XMax = (float) 9.2;
  public const float XMin = (float) -9.2;
}
