﻿using UnityEngine;

public static class Log {
    /// <summary>
    ///   <para>Logs a formatted simple message WITH TRANSLATION to the Unity console.</para>
    /// </summary>
    /// <param name="format">A composite format string.</param>
    /// <param name="args">Format arguments.</param>
    public static void Message (string format, params object[] args) {
        Debug.LogFormat(Tr.Go(format), args);
    }

    /// <summary>
    ///   <para>Logs a formatted warning message WITH TRANSLATION to the Unity console.</para>
    /// </summary>
    /// <param name="format">A composite format string.</param>
    /// <param name="args">Format arguments.</param>
    public static void Warning (string format, params object[] args) {
        Debug.LogWarningFormat(Tr.Go(format), args);
    }

    /// <summary>
    ///   <para>Logs a formatted error message WITH TRANSLATION to the Unity console.</para>
    /// </summary>
    /// <param name="format">A composite format string.</param>
    /// <param name="args">Format arguments.</param>
    public static void Error (string format, params object[] args) {
        Debug.LogErrorFormat(Tr.Go(format), args);
    }

    /// <summary>
    /// Log an error message and quit.
    /// </summary>
    /// <param name="message"></param>
    /// <param name="context"></param>
    public static void FatalError (string format, params object[] args) {
        Debug.LogErrorFormat(Tr.Go(format), args);
        Quit();
    }

    /// <summary>
    /// Quit the application whatever the current mode is.
    /// </summary>
    public static void Quit () {
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#elif UNITY_WEBPLAYER
        Application.OpenURL("http://google.com");
#else
        Application.Quit();
#endif
    }
}
