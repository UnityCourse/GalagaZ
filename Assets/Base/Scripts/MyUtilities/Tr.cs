﻿public static class Tr {
    /// <summary>
    /// translate a string in a given language.
    /// </summary>
    /// <param name="text">Text to translate</param>
    /// <param name="language">Language to translate in. "fr" (french) is default.</param>
    /// <returns>Translated text</returns>
    public static string Go (string text, string language = "fr") {
        // TODO implement translation table
        // for now, just return the english string given in parameter
        return text;
    }
}
