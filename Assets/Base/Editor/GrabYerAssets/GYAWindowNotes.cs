﻿#if UNITY_5 || UNITY_2017_1_OR_NEWER
#define UNITY_5_0_OR_NEWER
#endif

#if (UNITY_5_0_OR_NEWER && !UNITY_5_0)
#define UNITY_5_1_OR_NEWER
#endif

#if (UNITY_5_1_OR_NEWER && !UNITY_5_1)
#define UNITY_5_2_OR_NEWER
#endif

#if (UNITY_5_2_OR_NEWER && !UNITY_5_2)
#define UNITY_5_3_OR_NEWER
#endif

// Unity 5.3.4 and newer, auto assigns: UNITY_x_y_OR_NEWER

using UnityEngine;
using UnityEditor;
using System;


namespace XeirGYA
{

	public class GYAWindowNotes : EditorWindow
	{
		public GYAVars gyaVars = GYA.gyaVars; // GYA variables		
		public GYAData.Asset svCurrentPkg;// = GYA.svCurrentPkg;
		public int svCurrentPkgNumber;
		//public static string svCurrentPkgTitle = svCurrentPkg.title;// = GYA.svCurrentPkg;
		public ASPurchased.Result pur = null;
		public string noteTemp;

		internal Vector2 svPosition;

		internal static int toolbarInt; // Set default tab when opening window
		public string[] toolbarStrings = { "Package Data (Raw)" };

		public static void Init(int pVal = 1)
		{
			toolbarInt = pVal;
			bool isUtilityWindow = true;

			float width = 700f; // 300f
			float height = 320f; // 480f

			var winTitle = "GYA Asset Info:";// + svCurrentPkgTitle;
											 //var winTitle = svCurrentPkg.title + " (ID: " + svCurrentPkg.id + ")";
			var window = (GYAWindowNotes)EditorWindow.GetWindow(typeof(GYAWindowNotes), isUtilityWindow, winTitle, true);
			//wSettings window = GetWindow<wSettings>("GYA Settings", typeof(wSettings));
			window.minSize = new Vector2(width, height);
			window.maxSize = new Vector2(width, height);
			//window.CenterOnMainWin();
			//window.position = new Rect(GYA.GetWindow(typeof(GYA)).position);

		}

		public void OnEnable()
		{
			svCurrentPkg = GYA.svCurrentPkg;
			svCurrentPkgNumber = GYA.Instance.svCurrentPkgNumber;
			noteTemp = svCurrentPkg.AssetInfo.Notes;
			//Debug.Log(svCurrentPkgNumber);
			if (string.IsNullOrEmpty(svCurrentPkg.filePath))
				this.Close();
		}

		public void OnInspectorUpdate()
		{

		}

		void OnDestroy()
		{
			//GYA.Instance.Focus();
		}

		void OnGUI()
		{
			//toolbarInt = GUILayout.Toolbar (toolbarInt, toolbarStrings, GUILayout.Height(26f));
			switch (toolbarInt)
			{
				case 0:
					ShowAssetInfo();
					break;
				default:
					ShowAssetInfo();
					break;
			}

			//this.Repaint();
			//GYA.Instance.Focus();
		}

		void UpdateNote(int id)
		{
			//Debug.Log(svCurrentPkgNumber);
			//GYAExt.LogAsJson(GYA.gyaData.Assets[svCurrentPkgNumber].AssetInfo);
			//GYAExt.LogAsJson(svCurrentPkg.AssetInfo);

			svCurrentPkg.AssetInfo.id = svCurrentPkg.id;
			svCurrentPkg.AssetInfo.Notes = noteTemp;

			if (svCurrentPkg.AssetInfo.id == 0)
			{
				svCurrentPkg.AssetInfo.filePath = svCurrentPkg.filePath;
				GYA.gyaData.Assets[svCurrentPkgNumber].AssetInfo = svCurrentPkg.AssetInfo;
			}
			else
			{
				//GYA.gyaData.Assets[svCurrentPkgNumber].AssetInfo = svCurrentPkg.AssetInfo;
				GYA.gyaData.Assets.FindAll(x => x.id == id).ForEach(y => y.AssetInfo = svCurrentPkg.AssetInfo);
			}

			//if (svCurrentPkg.AssetInfo.Links == null)
			//{
			//	svCurrentPkg.AssetInfo.Links = new System.Collections.Generic.List<string>();
			//	svCurrentPkg.AssetInfo.Links.Add(string.Empty);
			//}
			//svCurrentPkg.AssetInfo.Links[0] = "test";

			// Update ALL ID's if there are multiple AS versions
			// Only edited entry is being updated in the SV
			//GYA.Instance.RefreshSV(); // does not work

			GYAFile.SaveGYAUserData();
			this.Close();
		}

		void ShowAssetInfo()
		{
			ShowPackageInfo();

			//EditorGUILayout.PrefixLabel("");
			EditorGUILayout.LabelField("Asset ID:\t\t\t" + svCurrentPkg.id);
			EditorGUILayout.LabelField("Asset Name (PKG):\t" + svCurrentPkg.title);
			if (pur != null) EditorGUILayout.LabelField("Asset Name (PAL):\t" + pur.name);

			EditorGUILayout.PrefixLabel("");
			//EditorGUILayout.HelpBox("Grab Yer Assets (GYA) Quick Reference ...", MessageType.None);
			EditorGUILayout.BeginHorizontal();

			EditorGUILayout.BeginVertical(GUILayout.Width(345));
			ShowPackagesGYA();
			//ShowPackagesPurchased();
			EditorGUILayout.EndVertical();

			EditorGUILayout.BeginVertical(GUILayout.Width(345));
			// Show notes field
			// -- Begin SV
			EditorGUILayout.BeginHorizontal();
			EditorGUILayout.LabelField("Asset Note:");
			if (GUILayout.Button("Save Note"))
			{
				// NOT WORKING, saved data is null

				UpdateNote(svCurrentPkg.id);
				//GYAFile.SaveGYAUserData();
			}
			EditorGUILayout.EndHorizontal();

			GUIStyle noteStyle = GUI.skin.GetStyle("TextArea");
			//noteStyle.richText = true;
			noteStyle.wordWrap = true;
			noteStyle.stretchHeight = true;
			//noteStyle.fontSize = fontSize;

			svPosition = EditorGUILayout.BeginScrollView(svPosition, GUILayout.Height(200));
			//noteTemp = EditorGUILayout.TextArea(noteTemp, GUILayout.ExpandHeight(true));
			noteTemp = EditorGUILayout.TextArea(noteTemp, noteStyle);
			EditorGUILayout.EndScrollView();

			//// Linux beta error fix: NullReferenceException ??
			//try
			//{
			//	svPosition = EditorGUILayout.BeginScrollView(svPosition, false, false, GUILayout.ExpandWidth(true));
			//}
			//catch (Exception)
			//{
			//}

			//// sv Content
			//         //ShowPackagesPurchased();
			////EditorGUILayout.DelayedTextField("Asset Notes", "Notes");
			////noteTemp = EditorGUILayout.TextArea(noteTemp, EditorStyles.wordWrappedLabel);
			//noteTemp = EditorGUILayout.TextArea(noteTemp);

			//// Linux beta error fix: NullReferenceException ??
			//try
			//{
			//	EditorGUILayout.EndScrollView();
			//}
			//catch (Exception)
			//{
			//}

			EditorGUILayout.EndVertical();

			EditorGUILayout.EndHorizontal();
			//EditorGUILayout.PrefixLabel("");
		}

		public void ShowPackageInfo()
		{
			//ShowPackagesGYA();
			pur = null;
			ShowPackagesPurchased();
		}

		public void ShowPackagesGYA()
		{
			string infoString =
				//"ID:\t\t" + svCurrentPkg.id +
				//"\nName:\t\t" + svCurrentPkg.title +
				"Unity Version:\t" + svCurrentPkg.unity_version +
				//"\nUpload ID:\t" + svCurrentPkg.upload_id +
				"\nVersion:\t\t" + svCurrentPkg.version +
				//"\nVersion ID:\t" + svCurrentPkg.version_id +
				"\n\nDate Package:\t" + svCurrentPkg.fileDataCreated +
				"\nDate File:  \t" + svCurrentPkg.fileDateCreated +
				"\n\nCategory ID:\t" + svCurrentPkg.category.id +
				"\nCategory Name:\t" + svCurrentPkg.category.label +
				"\nPublisher ID:\t" + svCurrentPkg.publisher.id +
				"\nPublisher Name:\t" + svCurrentPkg.publisher.label +
				//"\nLink ID:\t\t" + svCurrentPkg.link.id +
				//"\nLink Type:\t\t" + svCurrentPkg.link.type +
				//"\n\nCollection:\t" + svCurrentPkg.collection +
				"\nIs Deprecated:\t" + svCurrentPkg.isDeprecated +
				//"\n\nIs In a Group:\t" + svCurrentPkg.isInAGroup +
				//"\nIs Locked Version:\t" + svCurrentPkg.isInAGroupLockedVersion +
				"\n\nFile Size:\t\t" + svCurrentPkg.fileSize.BytesToKB() +
				"\nFile Path:\t\t" + svCurrentPkg.filePath;

			EditorGUILayout.BeginHorizontal();
			EditorGUILayout.LabelField("Copy To Clipboard:  ");
			if (GUILayout.Button("PKG Info"))
			{
				//GYAFile.CopyToClipboard(infoString);
				GYAFile.CopyToClipboard(svCurrentPkg);
			}
			if (pur != null)
			{
				if (GUILayout.Button("PAL Info"))
				{
					GYAFile.CopyToClipboard(pur);
				}
			}
			EditorGUILayout.EndHorizontal();

			EditorGUILayout.HelpBox(infoString, MessageType.None);
			//EditorGUILayout.HelpBox( GYAExt.ToJson(svCurrentPkg, true), MessageType.None);
		}

		public void ShowPackagesPurchased()
		{
			//ASPurchased.Result pur = null;
			//var tags = "";
			//string infoString = "";

			if (GYA.asPurchased.results != null)
			{
				//pur = GYA.asPurchased.results[0].items.Find( x => x.id == GYA.svCurrentPkg.id );
				pur = GYA.asPurchased.results.Find(x =>
						(x.id == svCurrentPkg.id)
				//&& (x.local_path.ToString() == svCurrentPkg.filePath)
				);
				//if (pur != null) tags = GYAExt.ToJson(pur.tags);
			}

			//       if (pur != null)
			//       {
			//           infoString =
			//               //"ID:\t\t" + pur.id +
			//"Name:\t\t" + pur.name +
			//        //"\nLocal Ver Name:\t" + pur.local_version_name +
			//        //"\nUser Rating:\t" + pur.user_rating + // int, 0-5
			//        //"\nStatus:\t\t" + pur.status + // string, published, deprecated
			//        //"\nIn Users Downloads:\t" + pur.in_users_downloads +
			//        "\n\nDate Purchased:\t" + pur.purchased_at + // date, 
			//        "\nDate Created:\t" + pur.created_at +
			//        "\nDate Published:\t" + pur.published_at +
			//        "\nDate Updated:\t" + pur.updated_at +
			//        "\nLast Downloaded:\t" + pur.last_downloaded_at +
			//        //"\n\nCategory ID:\t" + pur.category.id +
			//        //"\nCategory Name:\t" + pur.category.name +
			//        //"\nPublisher ID:\t" + pur.publisher.id +
			//        "\nPublisher Name:\t" + pur.publisher.name +
			//        //"\n\nCan Download:\t" + pur.can_download.ToBool() + // int, 1 true, 0 false
			//        //"\nCan Update:\t" + pur.can_update.ToBool() +
			//        //"\nComplete Project:\t" + pur.is_complete_project.ToBool() +
			//        //"\n\nType:\t\t" + pur.type +
			//        "\nTags:\t\t" + tags +
			//        //"\n\nLocal Path:\t" + pur.local_path +
			//        "\n\nIcon:\t" + pur.icon;
			//}

			//EditorGUILayout.BeginHorizontal();
			//EditorGUILayout.LabelField("Purchased Asset (PAL) Info:");
			//if (pur != null)
			//{
			//    if (GUILayout.Button("Copy To Clipboard"))
			//    {
			//        GYAFile.CopyToClipboard(pur);
			//    }
			//}
			//EditorGUILayout.EndHorizontal();


			//EditorGUILayout.HelpBox(pur != null ? infoString : "N/A", MessageType.None);

		}

		public void ShowPackagesLocal()
		{
			// asPackages id can be either int (ID number) OR string (filePath)
			//var pkg = GYA.asPackages.results.Find( x => (Convert.ToInt32(x.id)) == pkgID );
			ASPackageList.Result pkg = null;
			if (GYA.asPackages.results != null)
			{
				// Exported = path, AS = id
				if (svCurrentPkg.isExported)
					pkg = GYA.asPackages.results.Find(x => x.id == svCurrentPkg.filePath);
				else
					pkg = GYA.asPackages.results.Find(x =>
						(GYAExt.IntOrZero(x.id) == svCurrentPkg.id)
					);
			}

			EditorGUILayout.LabelField("Packages (Local) Info:");

			if (pkg != null)
			{
				EditorGUILayout.HelpBox(
					"ID:\t\t" + pkg.id +
					"\nTitle:\t\t" + pkg.title +
					"\nVersion:\t\t" + pkg.version +
					"\nVersion ID:\t" + pkg.version_id +
					"\nPublishe Date:\t" + pkg.pubdate +

					// root of each is null for standard assets
					"\n\nLink ID:\t\t" + (pkg.link != null ? pkg.link.id : null) +
					"\nLink Type:\t\t" + (pkg.link != null ? pkg.link.type : null) +
					"\nCategory ID:\t" + (pkg.category != null ? pkg.category.id : null) +
					"\nCategory Label:\t" + (pkg.category != null ? pkg.category.label : null) +
					"\nPublisher ID:\t" + (pkg.publisher != null ? pkg.publisher.id : null) +
					"\nPublisher Label:\t" + (pkg.publisher != null ? pkg.publisher.label : null) +
					"\n\nLocal Path:\t" + pkg.local_path
					//+ "\n\n" + pkg.local_icon
					//"\n\n" + tImage
					+ "", MessageType.None);
			}
		}
	}

}