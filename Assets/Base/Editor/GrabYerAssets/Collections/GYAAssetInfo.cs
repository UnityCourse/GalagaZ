﻿using System;
using System.Collections.Generic;
using GYAInternal.Json;

//using UnityEngine;
//using System.Collections;
//using GYAInternal.Json.Converters;

// GYA Asset Info - User maintained asset info for each package

namespace XeirGYA
{
    public class GYAAssetInfo
    {
		public List<AssetInfo> Data { get; set; }

        public GYAAssetInfo()
        {
			Data = new List<AssetInfo>();
        }

        public class AssetInfo
        {
            [JsonConverter(typeof(GYA.StringToIntConverter))]
            public int id { get; set; }
            public string filePath { get; set; }

			public List<string> Links { get; set; }
            public string Notes { get; set; }
			//public string Tags { get; set; }

			//public DateTimeOffset purchaseDate { get; set; }
			//[JsonConverter(typeof(GYA.StringToIntConverter))]
			//public int purchasePrice { get; set; }

			public AssetInfo()
            {
                id = 0;
                filePath = String.Empty;

				Links = new List<string>();
                Notes = String.Empty;
				//Tags = String.Empty;

                //purchaseDate = DateTimeOffset.MinValue;
                //purchasePrice = 0;
            }
        }
    }
}